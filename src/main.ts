import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { MicroserviceOptions, Transport } from "@nestjs/microservices";
import { join } from "path";
import { Envs } from "./config/envs";

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(AppModule, {
    transport: Transport.GRPC,
    options: {
      url: Envs.GRPC_CONNECTION_URL,
      protoPath: join(__dirname, "proto/statistic_generation.proto"),
      package: "statistic_generation",
    },
  });


  await app.listen();
  // eslint-disable-next-line no-console
  console.log(`Microservice is running on: ${Envs.GRPC_CONNECTION_URL}`);
}

bootstrap();