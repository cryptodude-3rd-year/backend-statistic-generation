export class EnumUtils {
  public static getEnumValues<T = number | string | unknown>(
    enumm: Record<string, string | number | unknown>,
  ): T[] {
    const values = Object.values(enumm);
    return values.slice(values.length / 2) as T[];
  }

  getKeyByValue(enumType, value: string): string {
    const indexOfS = Object.values(enumType).indexOf(value);
    return Object.keys(enumType)[indexOfS];
  }
}
