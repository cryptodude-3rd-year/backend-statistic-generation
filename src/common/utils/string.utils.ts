export class StringUtils {
  public static decodeString(
    str: string,
    fromEncoding: BufferEncoding,
    toEncoding: BufferEncoding,
  ): string {
    return Buffer.from(str, fromEncoding).toString(toEncoding);
  }

  public static isNotEmptyString(str?: unknown): boolean {
    return Boolean(str && typeof str === "string" && str.trim());
  }

  public static lengthNoMoreThan(str: string, length: number): boolean {
    return str.length <= length;
  }

  public static arrayToString(arr: string[], separator = ","): string {
    return arr.map((element) => `'${element}'`).join(separator);
  }

  public static fromSet<T>(set: Set<T>, separator = ", "): string {
    return Array.from(set).join(separator);
  }

  public static concatIf(
    str: string | number,
    strIf: string | number,
    separator: string | number,
  ): string {
    return strIf ? `${str}${separator}${strIf}` : `${str}`;
  }

  static splitStringIntoWordsForTsVector(str: string): string {
    let qString = StringUtils.isNotEmptyString(str) ? str.split(" ").filter(item => item).join("|") : "";
    while(qString[qString.length - 1] === "|") qString = qString.slice(0, -1);
    return qString;
  }

  static UTCOffsetToFloatOrNull(str: string): number {
    const UTCOffsetRegExp = new RegExp(StringRegExps.UTC_OFFSET);
    return UTCOffsetRegExp.test(str) ?
      Number(
        str
          .replace(",", ".")
          .substring(4),
      ) :
      null;
  }
}

export enum StringRegExps {
  UTC_OFFSET = "UTC[+-]\\d+(\\.\\d+)?"
}
