
export class BooleanUtils {
  public static isStringTrue(value: unknown): boolean {
    return value === true || String(value).toLowerCase() === "true" || value === "1" || value === 1;
  }

  public static strToBoolWithDefault(value, defaultValue = false): boolean {
    return value === undefined || value === null
      ? defaultValue
      : BooleanUtils.isStringTrue(value);
  }
}
