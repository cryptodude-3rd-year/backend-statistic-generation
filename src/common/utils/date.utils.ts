export class DateUtils {
	public static readonly DateConfigs: DateConfigType = {
	  FULL: {
	    hour: "numeric",
	    day: "2-digit",
	    month: "2-digit",
	    year: "numeric",
	    weekday: "long",
	  },
	  DATE_MONTH: { day: "2-digit", month: "2-digit" },
	};
	public static readonly MIN_IN_MS = 60000;
	public static readonly HOUR_IN_MS = DateUtils.MIN_IN_MS * 60;
	public static readonly DAY_IN_MS = DateUtils.HOUR_IN_MS * 24;

	static toCustomDate(date: Date | number | string, options: Intl.DateTimeFormatOptions): string {
	  return new Date(date).toLocaleDateString("ru-RU", options);
	}

	static dateInterval(date1: Date, date2: Date, options: Intl.DateTimeFormatOptions): string {
	  const dateFrom = DateUtils.toCustomDate(date1, options);
	  const dateTo = DateUtils.toCustomDate(date2, options);
	  return `${dateFrom}-${dateTo}`;
	}

	static isDateBetween(startsAt: Date, endsAt: Date, date: Date): boolean {
	  return (date >= endsAt && date <= startsAt) || (date <= endsAt && date >= startsAt);
	}

	static isNowBetween(startsAt: Date, endsAt: Date): boolean {
	  return DateUtils.isDateBetween(startsAt, endsAt, DateUtils.now());
	}

	public static readonly tomorrow = (): Date => DateUtils.addDays(DateUtils.now(), 1);
	public static readonly now = (): Date => new Date();
	public static readonly nowWithoutDate = (): Date =>
	  new Date(
	    +DateUtils.changeDate(DateUtils.now(), {
	      hours: 0,
	      minutes: 0,
	      seconds: 0,
	      ms: 0,
	    }) -
				DateUtils.now().getTimezoneOffset() * DateUtils.MIN_IN_MS,
	  );

	public static readonly nowUTC = (): Date =>
	  new Date(
	    DateUtils.now().getTime() + DateUtils.now().getTimezoneOffset() * DateUtils.MIN_IN_MS,
	  );

	static today(changeDate: ChangeDateType): Date {
	  return DateUtils.changeDate(new Date(), changeDate);
	}

	static changeDate(
	  dateToChange: Date,
	  { year, month, date, hours, minutes, seconds, ms }: ChangeDateType,
	): Date {
	  if (!dateToChange) {
	    return dateToChange;
	  }
	  const updatedDate = new Date();
	  updatedDate.setFullYear(
	    year ?? dateToChange.getFullYear(),
	    month ?? dateToChange.getMonth(),
	    date ?? dateToChange.getDate(),
	  );
	  updatedDate.setHours(
	    hours ?? dateToChange.getHours(),
	    minutes ?? dateToChange.getMinutes(),
	    seconds ?? dateToChange.getSeconds(),
	    ms ?? dateToChange.getMilliseconds(),
	  );
	  return updatedDate;
	}

	static addHours(oldDate: Date, hours: number): Date {
	  const newDate = new Date(oldDate);
	  newDate.setTime(newDate.getTime() + hours * DateUtils.HOUR_IN_MS);
	  return newDate;
	}

	static addDays(oldDate: Date, days: number): Date {
	  return DateUtils.addHours(oldDate, days * 24);
	}

	public static isDate(date: Date): boolean {
	  return date instanceof Date && !isNaN(+date);
	}

	static isDateAfter(date1: Date, date2: Date = new Date()): boolean {
	  return +date1 >= +date2;
	}

	static millisecondsBetween(date1: Date | number, date2: Date | number): number {
	  return +date1 - +date2;
	}

	static getCurrentYear(): number {
	  return new Date().getFullYear();
	}

	static currentMonthName(): string {
	  return DateUtils.now().toLocaleString("ru", { month: "long" });
	}

	static toStringWithoutTime(day: Date): string {
	  return day.toISOString().split("T")[0];
	}
}

export type ChangeDateType = Partial<{ year; month; date; hours; minutes; seconds; ms }>;

type DateConfigType = {
	[key: string]: Intl.DateTimeFormatOptions;
};
