import { IsArray, IsInt, IsOptional, ValidateNested } from "class-validator";
import { Type } from "class-transformer";

export class GetRatesByFilterRequestDto {
  @ValidateNested()
  @Type(() => Date)
  start_time: Date;

  @ValidateNested()
  @Type(() => Date)
  end_time: Date;

  @IsOptional()
  @IsArray()
  @IsInt({ each: true })
  exchange_ids?: number[];

  @IsOptional()
  @IsArray()
  @IsInt({ each: true })
  buy_currency_ids?: number[];

  @IsOptional()
  @IsArray()
  @IsInt({ each: true })
  sell_currency_ids?: number[];
}
