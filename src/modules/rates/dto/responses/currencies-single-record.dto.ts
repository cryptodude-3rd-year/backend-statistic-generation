import { IsBoolean, IsNumber, IsString } from "class-validator";

export class CurrenciesSingleRecordDto {
  @IsNumber()
  id: number;

  @IsString()
  name: string;

  @IsString()
  code_name: string;

  @IsString()
  logo: string;

  @IsBoolean()
  is_active: boolean;
}
