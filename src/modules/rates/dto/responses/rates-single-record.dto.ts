import { IsNumber, IsString, ValidateNested } from "class-validator";
import { ExchangesSingleRecordDto } from "./exchanges-single-record.dto";
import { CurrenciesSingleRecordDto } from "./currencies-single-record.dto";

export class RatesSingleRecordDto {
  @IsNumber()
  id: number;

  @IsNumber()
  sell_currency_id: number;

  @IsNumber()
  buy_currency_id: number;

  @IsNumber()
  exchange_id: number;

  @IsNumber()
  price: number;

  @IsString()
  time: string;

  @ValidateNested()
  sell_currency: CurrenciesSingleRecordDto;

  @ValidateNested()
  buy_currency: CurrenciesSingleRecordDto;

  @ValidateNested()
  exchange: ExchangesSingleRecordDto;
}
