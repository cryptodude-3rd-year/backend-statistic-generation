import { IsBoolean, IsNumber, IsString } from "class-validator";

export class ExchangesSingleRecordDto {
  @IsNumber()
  id: number;

  @IsString()
  name: string;

  @IsString()
  url: string;

  @IsString()
  logo: string;

  @IsNumber()
  maker_fee: number;

  @IsNumber()
  taker_fee: number;

  @IsString()
  fee_url: string;

  @IsBoolean()
  is_active: boolean;
}
