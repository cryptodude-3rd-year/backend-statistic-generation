import { GetRatesByFilterRequestDto } from "./dto/requests/get-rates-by-filter-request.dto";
import { RatesSingleRecordDto } from "./dto/responses/rates-single-record.dto";
import { Observable } from "rxjs";

export interface RatesServiceInterface {
  getRatesByFilter(GetRatesByFilter: GetRatesByFilterRequestDto): Observable<RatesSingleRecordDto[]>;
}