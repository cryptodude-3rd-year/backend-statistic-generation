import { Module } from "@nestjs/common";
import { DashboardsService } from "./dashboards.service";
import { ClientProxyFactory, Transport } from "@nestjs/microservices";
import { join } from "path";
import { Envs } from "../../config/envs";

@Module({
  imports: [],
  controllers: [DashboardsService],
  providers: [
    {
      provide: "DASHBOARD_PACKAGE",
      useFactory: () => {
        return ClientProxyFactory.create({
          transport: Transport.GRPC,
          options: {
            package: "dashboard",
            protoPath: join(__dirname, "../../proto/dashboard.proto"),
            url: Envs.USER_MANAGER_GRPC_CONNECTION_URL,
          },
        });
      },
    },
  ],
},
)
export class DashboardsModule {
}