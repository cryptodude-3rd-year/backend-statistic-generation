import { IsArray, IsNumber, IsOptional } from "class-validator";

export class DashboardResponseDto {
  @IsNumber()
  id: number;

  @IsNumber()
  user_id: number;

  @IsOptional()
  @IsNumber()
  period_start_time?: number;

  @IsOptional()
  @IsNumber()
  period_end_time?: number;

  @IsArray()
  @IsNumber({}, { each: true })
  exchanges: number[];

  @IsArray()
  @IsNumber({}, { each: true })
  buy_currencies: number[];

  @IsArray()
  @IsNumber({}, { each: true })
  sell_currencies: number[];

  @IsNumber()
  created_time: number;

  @IsNumber()
  updated_time: number;
}