import { Controller, Inject, OnModuleInit } from "@nestjs/common";
import { ClientGrpc, GrpcMethod } from "@nestjs/microservices";
import { DashboardServiceInterface } from "./dashboard.service.interface";
import { lastValueFrom } from "rxjs";

@Controller()
export class DashboardsService implements OnModuleInit {
  private dashboardService: DashboardServiceInterface;

  constructor(@Inject("DASHBOARD_PACKAGE") private client: ClientGrpc) {
  }

  onModuleInit() {
    this.dashboardService = this.client.getService<DashboardServiceInterface>("Dashboard");
  }

  @GrpcMethod("StatisticGenerationService", "GetDashboardByIDs")
  async getDashboardByIDs(ids: number[]) {
    const dashboards = await lastValueFrom(this.dashboardService.getDashboard({}));
    return dashboards.data.map((dashboard) => ids.includes(dashboard["id"]));
  }
}