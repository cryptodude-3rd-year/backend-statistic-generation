import { DashboardResponseDto } from "./dto/responses/dashboard-response.dto";
import { Observable } from "rxjs";

export interface DashboardServiceInterface {
  getDashboard(params: unknown): Observable<{ data: DashboardResponseDto[] }>;
}