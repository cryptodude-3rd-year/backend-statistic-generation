import { Module } from "@nestjs/common";
import { ClientProxyFactory, Transport } from "@nestjs/microservices";
import { join } from "path";
import { Envs } from "../../config/envs";
import { HotPricesService } from "./hot-prices.service";

@Module({
  imports: [],
  controllers: [HotPricesService],
  providers: [
    {
      provide: "DATA_STORAGE_PACKAGE",
      useFactory: () => {
        return ClientProxyFactory.create({
          transport: Transport.GRPC,
          options: {
            package: "data_storage",
            protoPath: join(__dirname, "../../proto/data_storage.proto"),
            url: Envs.DATA_STORAGE_GRPC_CONNECTION_URL,
          },
        });
      },
    },
  ],
},
)
export class HotPricesModule {
}