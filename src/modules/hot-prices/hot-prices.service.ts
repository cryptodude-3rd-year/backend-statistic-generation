import { Controller, Inject, OnModuleInit } from "@nestjs/common";
import { ClientGrpc, GrpcMethod } from "@nestjs/microservices";
import { ObjectUtils } from "../../common/utils/object.utils";
import { RatesServiceInterface } from "../rates/rates.service.interface";
import { lastValueFrom } from "rxjs";

@Controller()
export class HotPricesService implements OnModuleInit {
  private ratesService: RatesServiceInterface;

  constructor(@Inject("DATA_STORAGE_PACKAGE") private client: ClientGrpc) {
  }

  onModuleInit() {
    this.ratesService = this.client.getService<RatesServiceInterface>("Rates");
  }

  @GrpcMethod("StatisticGenerationService", "GetHotPrices")
  async getHotPrices() {
    const endDate = new Date(); // today
    const startDate = new Date(new Date().setDate(new Date().getDate() - 1)); // yesterday

    const rates = await lastValueFrom(this.ratesService.getRatesByFilter(
      {
        start_time: startDate,
        end_time: endDate,
      },
    ),
    );

    const groupedRates = ObjectUtils.groupBy({
      Group: rates,
      By: ["sell_currency_id", "buy_currency_id", "exchange_id"],
    });

    // сортировка по дате ASC
    groupedRates
      .forEach((group) => {
        group.sort((a, b) => {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          return new Date(a["time"]) - new Date(b["time"]);
        },
        );

      });

    const aggregatedWithDelta = groupedRates.map(
      (group) => {
        const length = group.length;

        const old_price = group[0]["price"];
        const new_price = group[length - 1]["price"];

        group[length - 1]["delta"] = +(((old_price - new_price) / new_price) * 100).toFixed(2);
        return group[length - 1];
      },
    );


    return aggregatedWithDelta.sort((a, b) => (b.delta - a.delta));
  }
}