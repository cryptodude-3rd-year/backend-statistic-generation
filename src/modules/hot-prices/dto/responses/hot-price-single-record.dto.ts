import { RatesSingleRecordDto } from "../../../rates/dto/responses/rates-single-record.dto";

export class HotPriceSingleRecordDto extends RatesSingleRecordDto {
  delta: number;
}