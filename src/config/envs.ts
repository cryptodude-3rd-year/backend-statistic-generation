/**
 * @author: valeevtr
 * @created: 01.12.2022
 * @Time: 17:25
 * @IDE: WebStorm
 */

import { config } from "dotenv";

config();

export const Envs = {
  // Server information
  GRPC_CONNECTION_URL: process.env.GRPC_CONNECTION_URL, // text: 0.0.0.0:50058
  DATA_STORAGE_GRPC_CONNECTION_URL: process.env.DATA_STORAGE_GRPC_CONNECTION_URL, // text: 0.0.0.0:50053
  USER_MANAGER_GRPC_CONNECTION_URL: process.env.USER_MANAGER_GRPC_CONNECTION_URL, // text: 0.0.0.0:50052

  // PostgreSQL's connection information
  // PG_HOST: process.env.PG_HOST, //				text: host name -> localhost
  // PG_PORT: NumbersUtils.toNumberOrDefault(process.env.PG_PORT, 5432), // number: port -> 5432
  // PG_DATABASE: process.env.PG_DATABASE, //	text: database name -> example_database
  // PG_USERNAME: process.env.PG_USERNAME, //	text: username -> postgres
  // PG_PASSWORD: process.env.PG_PASSWORD, //	text: password -> zxcursed
  // PG_LOGGING: BooleanUtils.strToBoolWithDefault(process.env.PG_LOGGING, false), // bool: sql logs -> true
  // PG_MIGRATIONS_RUN: BooleanUtils.strToBoolWithDefault(process.env.PG_MIGRATIONS_RUN, false), // bool: is run migrations -> true
  //
  // // Local dev
  // JWT_PUBLIC_KEY: process.env.JWT_PUBLIC_KEY, // text: public key (base64) -> AAzizTiBQVUSchizoASzc0VOLS0tLS0KTUZrd0V3WUhLb1pMeowQ0FRWUlLb1pJemmyREFRY0RRZ0FFTEdBRGFpb0JsWnKJERSTI1eHduMVJYNUsxTzarNapaN2NagamasaFWSzlFVWaveDltQUNUQXptN04rSXd4bkdPSjlIbmZDRDhPCTFREE0KLS0tLS1FTkQgUVCElDIEtFWS0tLS0tCg
  //
  // // swagger information
  // IS_WRITE_SWAGGER_CONFIG: BooleanUtils.strToBoolWithDefault(
  //   process.env.IS_WRITE_SWAGGER_CONFIG,
  //   false,
  // ), // bool: is writing swagger config -> false
  // SWAGGER_HOSTS: [
  //   // hosts information
  //   {
  //     URL: `http://localhost:${process.env._PORT ?? 5000}`,
  //     DESCRIPTION: "localhost",
  //   },
  // ],
  //
  // // GRAYLOG
  // IS_USE_GRAYLOG: BooleanUtils.strToBoolWithDefault(process.env.IS_USE_GRAYLOG, false), // bool: is use graylog for saving logs => true
  // GRAYLOG_HOST: process.env.GRAYLOG_HOST, // text: graylog host for saving logs => localhost
  // GRAYLOG_PORT: NumbersUtils.toNumberOrDefault(process.env.GRAYLOG_PORT, 12201), // number: graylog port for saving logs => 12201
  // GRAYLOG_BUFFER_SIZE: NumbersUtils.toNumberOrDefault(
  //   process.env.GRAYLOG_BUFFER_SIZE,
//   262144
// ), // number: graylog recv_buffer_size => 322
};

