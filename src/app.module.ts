import { Module } from "@nestjs/common";
import { DashboardsModule } from "./modules/dashboards/dashboards.module";
import { HotPricesModule } from "./modules/hot-prices/hot-prices.module";


@Module({
  imports: [
    DashboardsModule,
    HotPricesModule,
  ],
  providers: [],
})
export class AppModule {
}
