FROM node:16.18.0-alpine AS base

RUN npm i -g pnpm@7.25.1

FROM base AS dependencies

WORKDIR /app
COPY package.json pnpm-lock.yaml tsconfig.build.json ./
RUN pnpm install --frozen-lockfile

FROM base AS build

WORKDIR /app
COPY . .
COPY --from=dependencies /app/node_modules ./node_modules
RUN pnpm build
RUN pnpm config set ignore-scripts true
RUN pnpm prune --prod

FROM base AS deploy

WORKDIR /app
COPY --from=build /app/pnpm-lock.yaml ./pnpm-lock.yaml
COPY --from=build /app/nest-cli.json ./nest-cli.json
COPY --from=build /app/tsconfig.json ./tsconfig.json
COPY --from=build /app/tsconfig.build.json ./tsconfig.build.json
COPY --from=build /app/package.json ./package.json
COPY --from=build /app/node_modules ./node_modules
COPY --from=build /app/dist ./dist

CMD ["pnpm", "start:prod"]