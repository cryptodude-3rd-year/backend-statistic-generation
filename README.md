<p align="center">
  <a href="https://gitlab.com/cryptodude-3rd-year/backend-statistic-generation" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

- node.js

```bash
$ npm -g i pnpm
$ pnpm i
```

- Docker

```bash
$ docker build -t nest-pnpm-docker .
$ docker run -dp 3000:3000 nest-pnpm-docker
```

## Running the app

```bash
# development
$ pnpm run start

# watch mode
$ pnpm run start:dev

# production mode
$ pnpm run start:prod
```

## Test

```bash
# unit tests
$ pnpm run test

# e2e tests
$ pnpm run test:e2e

# test coverage
$ pnpm run test:cov
```

## Eslint

```bash
# lint tests
$ pnpm run eslint:test

# lint fix
$ pnpm run eslint:fix
```

## docker-compose

### Graylog

- Login: `admin`
- Password: `admin`

## Stay in touch

|          Role           |                         Username                          |
|:-----------------------:|:---------------------------------------------------------:|
|      **Developer**      |    [valtage](https://gitlab.com/tagir_valeev)             |

## file and code template setting

```javascript
/**
 * @author: ${USER}
 * @created: ${DATE}
 * @Time: ${TIME}
 * @IDE: ${PRODUCT_NAME}
 */
```

## Folders

```text
├── bin // Custom tasks
├── .husky // Husky commit hooks
├── dist // Source build
├── src
|   ├── services // Services
|   ├── config // Environment Configuration
│   │   ├── envs.ts // environment variables
│   │   ├── app
│   │   ├── database
│   │   │   └── postgres
|   ├── database
|   │   ├── factories
│   │   ├── migrations
│   │   └── seeders
|   ├── common // Global Module
|   │   ├── database // Custom Typeorm methods
|   │   ├── types // Custom Nest types
|   │   ├── constants // Constant value and Enum
|   │   ├── decorators // Nest Decorators
|   │   │   ├── metadata // Metadata Decorators
|   │   │   └── requests // Requests Decorators
|   │   ├── dto // DTO (Data Transfer Object) Schema, Validation
|   │   ├── exceptions // Nest Exceptions
|   │   ├── guards // Nest Guards
|   │   ├── helpers // Nest helpers
|   │   │   ├── exceptions
|   │   │   └── responses
|   │   ├── interfaces // TypeScript Interfaces
|   │   ├── middlewares // Nest middlewares
|   ├── modules // Module structure
|   |   ├── dto // Module DTOs
|   |   |   ├── base // Module base DTOs
|   |   |   ├── dto // Module requests DTOs
|   |   |   └── dto // Module responses DTOs
|   |   ├── entities // Module entitiess
|   |   ├── repositories // Module repositories
|   |   ├── services // Module services
|   |   ├── controllers // Module controllers
|   |   └──types // Module types
|   ├── app.module.ts
|   └── * // Other Nest Modules, non-global, same as common structure above
├── test // Jest testing
└── typings // Modules and global type definitions
```

### For fast replace get/set:

replase:
`([gs]et) ([a-zA-Z])((.*)_([a-z]))?`

to:
`$1\U$2\E$4\U$5`

and replace:
`) private`

to:

```
)
    private
```
